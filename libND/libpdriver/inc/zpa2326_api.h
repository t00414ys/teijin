/*!******************************************************************************
 * @file    zpa2326_api.h
 * @brief   zpa2326 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ZPA2326_API_H__
#define __ZPA2326_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int zpa2326_init( unsigned int param );
void zpa2326_ctrl( int f_ena );
unsigned int zpa2326_rcv( unsigned int tick );
int zpa2326_conv( frizz_fp* press_data );

int zpa2326_setparam( void *ptr );
unsigned int zpa2326_get_ver( void );
unsigned int zpa2326_get_name( void );

int zpa2326_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif // __ZPA2326_H__

