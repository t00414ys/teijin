/*!******************************************************************************
 * @file    pa12201001_api.h
 * @brief   pa12201001 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PA12201001_API_H__
#define __PA12201001_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int pa12201001_init( unsigned int param );

void pa12201001_ctrl_prox( int f_ena );
void pa12201001_ctrl_light( int f_ena );

unsigned int pa12201001_rcv_prox( unsigned int tick );
int pa12201001_conv_prox( frizz_fp *prox_data );

unsigned int pa12201001_rcv_light( unsigned int tick );
int pa12201001_conv_light( frizz_fp *light_data );

int pa12201001_setparam( void *ptr );
unsigned int pa12201001_get_ver( void );
unsigned int pa12201001_get_name( void );

int pa12201001_light_get_condition( void *data );
int pa12201001_prox_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif // __PA12201001_API_H__

