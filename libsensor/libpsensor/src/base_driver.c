/*!******************************************************************************
 * @file    base_driver.c
 * @brief   base phy driver prototype
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_util.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "sensor_if.h"
#include "base_driver.h"
#include "config.h"
#include "config_type.h"

#define	D_MAX_DEVICE_ID					(0x0FF)
static unsigned char	raw_device_condition[D_MAX_DEVICE_ID];
extern	unsigned int	g_accel_name;
extern	unsigned int	g_gyro_name;
extern	unsigned int	g_magn_name;
extern	unsigned int	g_pres_name;

// BOARD_TYPE_TABLE
typedef struct {
	BOARD_TYPE		type;					// board type
	unsigned int	accel_name;				// use accel name
	unsigned int	gyro_name;				// use gyro name
	unsigned int	magn_name;				// use magn name
	unsigned int	pres_name;				// use pres name
	unsigned long	clock;					// clock
} BOARD_TYPE_TABLE;

BOARD_TYPE_TABLE	board_type_tbl[] = {
	{	D_BOARD_CHIGNON2,	D_DRIVER_NAME_MPUXXXX,	D_DRIVER_NAME_MPUXXXX,	D_DRIVER_NAME_AK0991X,	D_DRIVER_NAME_ZPA2326,	40000000	},
	{	D_BOARD_CHIGNONB,	D_DRIVER_NAME_BMI160,	D_DRIVER_NAME_BMI160,	D_DRIVER_NAME_BMM150,	D_DRIVER_NAME_BMP280,	38400000	},
	{	D_BOARD_AYAME,		D_DRIVER_NAME_MPUXXXX,	D_DRIVER_NAME_MPUXXXX,	D_DRIVER_NAME_MMC3516,	D_DRIVER_NAME_BMP280,	40000000	},
	{	D_BOARD_AYAME_KURO,	D_DRIVER_NAME_MPUXXXX,	D_DRIVER_NAME_MPUXXXX,	D_DRIVER_NAME_AK0991X,	D_DRIVER_NAME_BMP280,	40000000	}
};

unsigned char get_device_condition( int device_id )
{
	return raw_device_condition[device_id];
}

void set_device_condition_regist( int device_id )
{
	raw_device_condition[device_id] |= CONDITION_REGIST;
	return;
}

void set_device_condition_initerr( int device_id )
{
	raw_device_condition[device_id] |= CONDITION_INITERR;
	return;
}


void set_device_condition_phyerr( int device_id )
{
	raw_device_condition[device_id] |= CONDITION_PHYERR;
	return;
}

void reset_device_condition_phyerr( int device_id )
{
	raw_device_condition[device_id] &= ( ~CONDITION_PHYERR );
	return;
}


pdriver_if_t *basedevice_init( pdriver_if_t *dev, int size, int device_id )
{
	int				i;

	set_device_condition_regist( device_id );
	raw_device_condition[device_id] |= CONDITION_PHYDEV;

	for( i = 0 ; i < size ; i++ ) {
		if( dev->init != 0 ) {
			if( RESULT_SUCCESS_INIT == ( *dev->init )( dev->data ) ) {
				return dev;
			}
		} else {
			if( size <= 1 ) {
				raw_device_condition[device_id] |= CONDITION_PHYNODEV;
			} else {
				set_device_condition_initerr( device_id );
			}
			return 0;
		}
		dev++;
	}
	return 0;
}


unsigned long check_board_clock( void )
{
#if		defined(__CONFIG_CHIGNONB_H__)
	int		i;
	for( i = 0 ; i < NELEMENT( board_type_tbl ) ; i++ ) {
		if( ( board_type_tbl[i].accel_name == g_accel_name ) &&
			( board_type_tbl[i].gyro_name  == g_gyro_name )  &&
			( board_type_tbl[i].magn_name  == g_magn_name )  &&
			( board_type_tbl[i].pres_name  == g_pres_name ) ) {
			return board_type_tbl[i].clock;
		}
	}
#endif
	return 0;
}


#if	defined(RUN_ON_PC_EXEC) || defined(RUN_ON_XPLORER)	// EMULATION
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <getopt.h>

struct st_ExecOptions g_ExecOptions;

void OptionsAnalysis( int argc,  char * const *argv )
{
	int c,in;

	memset( &g_ExecOptions, 0, sizeof( g_ExecOptions ) );
	in = 0;
	while( ( c = getopt( argc, argv, "p:a:c:e:n:o:" ) ) != -1 ) {
		switch( c ) {
		case 'p':
			if( optarg != NULL ) {
				strncpy( g_ExecOptions.LogFile_PPG, optarg, sizeof( g_ExecOptions.LogFile_PPG ) - 1 );
				in = 1;
			}
			break;
		case 'a':
			if( optarg != NULL ) {
				strncpy( g_ExecOptions.LogFile_9AXIS, optarg, sizeof( g_ExecOptions.LogFile_9AXIS ) - 1 );
				in = 1;
			}
			break;
		case 'c':
			if( optarg != NULL ) {
				strncpy( g_ExecOptions.LogFile_COMMAND, optarg, sizeof( g_ExecOptions.LogFile_COMMAND ) - 1 );
				in = 1;
			}
			break;
		case 'o':
			if( optarg != NULL ) {
				strncpy( g_ExecOptions.LogFile_OUT, optarg, sizeof( g_ExecOptions.LogFile_OUT ) - 1 );
			}
			break;
		case 'e':
			if( optarg != NULL ) {
				g_ExecOptions.TermCondition = ( eTERM_EVENT ) atoi( optarg );
			}
			break;
		case 'n':
			if( optarg != NULL ) {
				g_ExecOptions.TermCount = atoi( optarg );
				if( g_ExecOptions.TermCount < 0 ) {
					g_ExecOptions.TermCount = 1;
				}
			}
			break;
		default:
			printf( "usage: %s [-p PPG-LOG-FILE] [-a 9AXIS LOG FILE] [-c COMMAND LOG FILE] [-e {0/1/2}] [-n {loop count]] [-o data out dir]\n", argv[0] );
			printf( "   -e : termination condition  0:INFINITY, 1:LOG FILE END, 2:LOOP COUNT\n" );
			exit( 1 );
			break;
		}
	}
	if( in == 0 ) {
		printf( "usage: %s [-p PPG-LOG-FILE] [-a 9AXIS LOG FILE] [-c COMMAND LOG FILE] [-e {0/1/2}] [-n {loop count]] [-o data out dir]\n", argv[0] );
		printf( "   -e : termination condition  0:INFINITY, 1:LOG FILE END, 2:LOOP COUNT\n" );
		exit( 1 );
	}

	printf( "*** Emulation condition ***\n" );
	if( g_ExecOptions.LogFile_COMMAND != 0 ) {
		printf( "   use COMMAND LOG [%s]\n", g_ExecOptions.LogFile_COMMAND );
	}
	if( g_ExecOptions.LogFile_PPG[0] != 0 ) {
		printf( "   use PPG LOG     [%s]\n", g_ExecOptions.LogFile_PPG );
	}
	if( g_ExecOptions.LogFile_9AXIS[0] != 0 ) {
		printf( "   use 9AXIS LOG   [%s]\n", g_ExecOptions.LogFile_9AXIS );
	}

	switch( ( int ) g_ExecOptions.TermCondition ) {
	case( int ) D_TERM_OF_INFINITY:
	default:
		printf( "   termination condition is INFINITY\n" );
		g_ExecOptions.TermCondition = D_TERM_OF_INFINITY;
		break;
	case( int ) D_TERM_OF_LOG_END:
		printf( "   termination condition is LOG FILE END\n" );
		g_ExecOptions.TermCondition = D_TERM_OF_LOG_END;
		break;
	case( int ) D_TERM_OF_COUNT:
		printf( "   termination condition is %d times of loops\n", g_ExecOptions.TermCount );
		g_ExecOptions.TermCondition = D_TERM_OF_COUNT;
		break;
	}
	g_ExecOptions.TermFlag = 0;
	return;
}


void LogFile_LineArrange( char *buffer , int size )
{
	char			*ptr;
	int				c;

	// whisout comment
	ptr = strchr( buffer, '#' );
	if( ptr ) {
		*ptr = 0;
	}

	// change '\t' to ' '
	ptr = buffer;
	c = size;
	while( ( *ptr != 0 ) || ( c > 0 ) ) {
		ptr = strchr( ptr, '\t' );
		if( ptr != 0 ) {
			*ptr = ' ';
			ptr++;
		} else {
			break;
		}
		c--;
	}

	// check data
	for( ptr = buffer ; *ptr != 0 ; ptr++ ) {
		if( *ptr != ' ' && *ptr != '\r' && *ptr != '\n' ) {
			break;
		}
	}

	if( *ptr != 0 ) {
		// change ',' to ' '
		ptr = buffer;
		c = size;
		while( ( *ptr != 0 ) && ( c > 0 ) ) {
			ptr = strchr( ptr, ',' );
			if( ptr != 0 ) {
				*ptr = ' ';
				ptr++;
			} else {
				break;
			}
			c--;
		}
	} else {
		*buffer = 0;
	}
}

#if	defined(RUN_ON_PC_EXEC)

void GraphPlot( int size, stGraphContent *data, char *file_name, int seq )
{
	FILE		*gp;
	int			i,j;
	char		buffer1[1024];
	char		buffer2[512];

	if( g_ExecOptions.LogFile_OUT[0] != 0 ) {
		/* ---- graph_plot ---- */
		sprintf( buffer1, "%s/%s_%08d.emf", g_ExecOptions.LogFile_OUT, file_name, seq );
		gp = popen( "gnuplot -persist", "w" );
		fprintf( gp, "set terminal emf\n" );
		fprintf( gp, "set output '%s'\n", buffer1 );
		// --------
		sprintf(buffer1,"plot ");
		for( i = 0 ; i < size ; i++ ) {
			sprintf(buffer2,"'-' with lines linetype %d title \"%s\" ", i+1,(data+i)->title );
			strcat(buffer1,buffer2);
			if( i != (size - 1) ) {
				strcat(buffer1,",");
			}
		}
		strcat(buffer1,"\n");
		fprintf( gp, "%s", buffer1 );
		// --------
		for( i = 0 ; i < size ; i++ ) {
			for( j = 0 ; j < (data+i)->elem ; j++ ) {
				fprintf( gp, "%f\n", (data+i)->data[j] );
			}
			fprintf( gp, "e\n" );
		}
		pclose( gp );
	}
}
#endif	// defined(RUN_ON_PC_EXEC)


#endif	// defined(RUN_ON_PC)
