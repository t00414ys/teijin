/*!******************************************************************************
 * @file    accl_raw.c
 * @brief   sample program for control accel raw data
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "sensor_if.h"
#include "accl_driver.h"
#include "if/accl_raw_if.h"
#include "frizz_math.h"
#include "frizz_const.h"
#include "gpio.h"

#define DEF_INIT(x) x ## _init

EXTERN_C sensor_if_t* DEF_INIT( accl_raw )( void );

/* accel phy sensor list */
static	pdriver_if_t	g_devif[] = {
		//ACCLEMU_DATA					// Emulation
		//ADXL362_DATA					// ADXL362
		ADS8332_DATA					// ADS8332
		//MC3413_DATA						// MC3413
		//BMA2XX_DATA						// BMA2XX
		//BMI160_DATA						// BMI160	(It is defined before the MPUXXXX_DATA) [I2C adder is the same ]
		//MPUXXXX_DATA					// MPU9255 or MPU6505
		//LSM330_DATA						// LSM330
		//LSM6DS3_DATA					// LSM6DS3
		//LIS2DH_DATA						// LIS2DH
		//STK8313_DATA					// STK8313
		//LIS2DS12_DATA					// LIS2DS12
		//TBLEND_DATA						// TBLEND
};
static	pdriver_if_t	*g_pDevIF;

typedef struct {
	// ID
	unsigned char		id;
	// IF
	sensor_if_t			pif;
	// status
	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	unsigned int		remain_total;
	// data
	frizz_fp			data[12];	//return するデータ数
} device_sensor_t;

static device_sensor_t g_device;
accel_raw_data_t accel_data;
unsigned int	g_accel_name = 0;


static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	return 0;
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 12;	//data[?]の?にそろえる
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		if( g_pDevIF->ctrl != 0 ) {
			( *g_pDevIF->ctrl )( f_active );
		}
		g_device.f_active = f_active;
		g_device.remain_total = 0;
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	// TODO: call to set device for update interval api
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		if( g_pDevIF->get_ver != 0 ) {
			ret = ( *g_pDevIF->get_ver )();
		}
		break;
	case DEVICE_GET_NAME:
		ret = g_accel_name;
		break;
	case SENSOR_SET_DIRECTION: {
		if( g_pDevIF->set_param != 0 ) {
			setting_direction_t param_direction;
			int *p = ( int* )param;
			param_direction.map_x = p[0];
			param_direction.map_y = p[1];
			param_direction.map_z = p[2];
			param_direction.negate_x = p[3];
			param_direction.negate_y = p[4];
			param_direction.negate_z = p[5];
			ret = ( *g_pDevIF->set_param )( &param_direction );
		} else {
			ret = -1;
		}
		break;
	}
	case SENSOR_ACCL_GET_REAL_RAW_DATA: {
		if( g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] != 0 ) {
			( *g_pDevIF->extra_function[INDEX_ACCL_GET_REAL_RAW_DATA] )( ( void* )param );
		}
		ret = 0;
		break;
	}
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	unsigned int remain = 0;
	if( g_pDevIF->recv != 0 ) {
		remain = ( *g_pDevIF->recv )( g_device.tick );
	}
	if( remain == 0 ) {
		remain = g_device.tick;
		g_device.ts = ts;
		g_device.f_need = 1;
	} else {
		g_device.remain_total = g_device.remain_total + remain;
		if( g_device.remain_total >= g_device.tick ) {
			g_device.remain_total = 0;
			g_device.ts = ts;
			g_device.f_need = 1;
		}
	}
	return ts + remain;
}

struct data {
	frizz_fp   accl[4];
	frizz_fp   gyro[4];
	frizz_fp   magn[4];
} tmp_data;


static int calculate( void )
{


#if 0
	int		result = 0;
	frizz_fp	*fz = ( frizz_fp* )&g_device.data;

	if( g_pDevIF->conv != 0 ) {
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		accel_data.data[0] =  fz[0];
		accel_data.data[1] =  fz[1];
		accel_data.data[2] =  fz[2];
	}
	g_device.f_need = 0;
	return result;
#else
	int		result = 0;
	//ads8332.c　からdata の読み込み
	//	frizz_fp	*fz = ( frizz_fp* )&g_device.data;
	frizz_fp *fz = (frizz_fp*)&tmp_data;


	if( g_pDevIF->conv != 0 ) {
		//result = ( *g_pDevIF->conv )( ( frizz_fp* )&g_device.data );
		result = ( *g_pDevIF->conv )( ( frizz_fp* )&tmp_data );

		//offset setting

		//gpio_set_data( GPIO_NO_3, 0 );


#if 0
		// calibration用セッティング
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (0.0f));// * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (0.0f));// * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (0.0f));// * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

#elif 0 //test
		//acceleration [g]
		g_device.data[0] =  fz[1] - as_frizz_fp( 0.00809392569208308f);
		g_device.data[1] =  fz[2] - as_frizz_fp( 0.00809392569208308f);;
		g_device.data[2] =  fz[3] - as_frizz_fp( 0.00809392569208308f);;
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99436067137529600f)  + as_float(fz[6]) * (-0.01103236189176390f) + as_float(fz[7]) * (-0.01107615849219060f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00809392569208308f)  + as_float(fz[6]) * ( 0.99211773384856900f) + as_float(fz[7]) * ( 0.01437190770943420f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00167110092839671f)  + as_float(fz[6]) * (-0.00094626957219948f) + as_float(fz[7]) * ( 1.00003484139071000f) - (0.0f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.00809392569208308f)), as_frizz_fp(1.00003484139071000f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.00809392569208308f)), as_frizz_fp(1.00003484139071000f));
		g_device.data[10] = frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.00809392569208308f)), as_frizz_fp(1.00003484139071000f));

#elif 1 //16G1500dps demo@tokyo (17F0149)
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00376345251159000f)  + as_float(fz[6]) * (-0.01334774957598880f) + as_float(fz[7]) * ( 0.02189197633860310f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * (-0.00396315396748842f)  + as_float(fz[6]) * ( 1.00488808697013000f) + as_float(fz[7]) * (-0.00185495190323416f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00146634868238452f)  + as_float(fz[6]) * ( 0.00535480627125384f) + as_float(fz[7]) * ( 0.98827794204398500f) - (0.0f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

#elif 0 //16G1500dps RICOH(17F0116)
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.98760725602766000f)  + as_float(fz[6]) * (-0.02604772084308950f) + as_float(fz[7]) * (-0.00173467675245195f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * (-0.00034868787768451f)  + as_float(fz[6]) * ( 1.01318011227359000f) + as_float(fz[7]) * (-0.00073628492032272f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.01706605354818040f)  + as_float(fz[6]) * ( 0.00298207035626530f) + as_float(fz[7]) * ( 0.99669560607700900f) - (0.0f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

#elif 0 //16G1500dps RICOH(17F0122)
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp(-0.043302215f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.082489027f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.060915228f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.98803368180670500f)  + as_float(fz[6]) * (-0.03632757216649990f) + as_float(fz[7]) * (-0.02909392967983780f) - (-0.008557734f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.03076134489528250f)  + as_float(fz[6]) * ( 1.01106277300709000f) + as_float(fz[7]) * ( 0.03249199094149190f) - (-0.324053273f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00830883174901036f)  + as_float(fz[6]) * ( 0.01096073757761330f) + as_float(fz[7]) * ( 1.01330633093241000f) - ( 6.069749453f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.1525f)), as_frizz_fp(0.424517133f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(-0.323f)), as_frizz_fp(0.419443223f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(-5.269f)), as_frizz_fp(0.39238237f));


#elif 0 //16G1500dps(SS24-1611-0002)demo
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00022872256462000f)  + as_float(fz[6]) * (-0.01783443939795130f) + as_float(fz[7]) * (-0.02378921120673800f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.02953212317325920f)  + as_float(fz[6]) * ( 0.98674495228283100f) + as_float(fz[7]) * ( 0.01924952635355670f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00717613025417910f)  + as_float(fz[6]) * (-0.00902123069050043f) + as_float(fz[7]) * ( 0.99868571654673200f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(651C120112/1B1602224D/17A0011)panasonic
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00228418217736000f)  + as_float(fz[6]) * (-0.03813805554267050f) + as_float(fz[7]) * (-0.02318792121789790f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.02009393428941320f)  + as_float(fz[6]) * ( 1.01142535613910000f) + as_float(fz[7]) * ( 0.01446629128107410f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00006977205411394f)  + as_float(fz[6]) * ( 0.00194755372179580f) + as_float(fz[7]) * ( 0.99913782291359000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0113//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99454670908884800f)  + as_float(fz[6]) * ( 0.00025634627565425f) + as_float(fz[7]) * (-0.00366622324843955f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01259270798615810f)  + as_float(fz[6]) * ( 1.02403428202025000f) + as_float(fz[7]) * (-0.00375408912525978f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00771582027571433f)  + as_float(fz[6]) * (-0.00036007636439527f) + as_float(fz[7]) * ( 0.96779177266460000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0182//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.05721712510410000f)  + as_float(fz[6]) * ( 0.00156716244999509f) + as_float(fz[7]) * ( 0.00469346760220740f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00907436380641966f)  + as_float(fz[6]) * ( 0.94835507189279600f) + as_float(fz[7]) * (-0.00231369017948599f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00197191559314943f)  + as_float(fz[6]) * (-0.00260579790525069f) + as_float(fz[7]) * ( 0.97594745820014300f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200Gdemo(17F0081//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.03999035652355000f)  + as_float(fz[6]) * ( 0.00408095325687137f) + as_float(fz[7]) * (-0.00369189627830701f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00907629479479384f)  + as_float(fz[6]) * ( 0.97671134370687700f) + as_float(fz[7]) * (-0.00847646450139187f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00019885431389206f)  + as_float(fz[6]) * (-0.00283850252681561f) + as_float(fz[7]) * ( 1.01366852327046000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200Gdemo(17F0013//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00069429141884000f)  + as_float(fz[6]) * (-0.00661331726876075f) + as_float(fz[7]) * (-0.00076749169094603f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01203543647012780f)  + as_float(fz[6]) * ( 0.94730330028169100f) + as_float(fz[7]) * ( 0.00299505803689769f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00415285059944077f)  + as_float(fz[6]) * (-0.00657474276333623f) + as_float(fz[7]) * ( 1.00136934655005000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0178//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.05469024124503000f)  + as_float(fz[6]) * (-0.00514180124865629f) + as_float(fz[7]) * (-0.00413360076458667f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00099812813484059f)  + as_float(fz[6]) * ( 0.95111201575703700f) + as_float(fz[7]) * (-0.00443755359523010f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.01549583467450380f)  + as_float(fz[6]) * ( 0.00120864773870475f) + as_float(fz[7]) * ( 0.99677029066994400f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0180//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.03298073534788000f)  + as_float(fz[6]) * (-0.00892702323382221f) + as_float(fz[7]) * ( 0.02871930072312720f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00672201237445775f)  + as_float(fz[6]) * ( 1.00667244774235000f) + as_float(fz[7]) * ( 0.01305435376465300f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.01819130694083720f)  + as_float(fz[6]) * (-0.02368659343659880f) + as_float(fz[7]) * ( 0.96517857283404000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0181//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00574818705784000f)  + as_float(fz[6]) * (-0.00443749941644303f) + as_float(fz[7]) * ( 0.00257556028426457f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00660077033542152f)  + as_float(fz[6]) * ( 0.95564312503439500f) + as_float(fz[7]) * (-0.00036887369089041f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00036366834602572f)  + as_float(fz[6]) * ( 0.00250356222925909f) + as_float(fz[7]) * ( 0.98326951174368600f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17C0010//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.03624524004475000f)  + as_float(fz[6]) * (-0.00051272414737974f) + as_float(fz[7]) * (-0.00257783649331189f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00054836029214562f)  + as_float(fz[6]) * ( 0.96260581042266500f) + as_float(fz[7]) * (-0.00706357951050564f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00002778048196204f)  + as_float(fz[6]) * (-0.00118718060693866f) + as_float(fz[7]) * ( 0.97191485076062200f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0092//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99142051273063300f)  + as_float(fz[6]) * (-0.00955728073475030f) + as_float(fz[7]) * ( 0.00299248849942066f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00556509945428394f)  + as_float(fz[6]) * ( 0.94220428192880300f) + as_float(fz[7]) * (-0.00507392152280167f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00462039864240574f)  + as_float(fz[6]) * (-0.00190670076639705f) + as_float(fz[7]) * ( 0.99166446180379900f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0058/1616170726/2617260050)上智大学
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.97752463814401400f)  + as_float(fz[6]) * (-0.00159165876822789f) + as_float(fz[7]) * (-0.00101787278906402f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00516053180727369f)  + as_float(fz[6]) * ( 0.95678710657537600f) + as_float(fz[7]) * (-0.00220364013394174f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00190420245481459f)  + as_float(fz[6]) * (-0.00246237830664039f) + as_float(fz[7]) * ( 0.99201313344838500f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //5G(17F0112//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00231990915200000f)  + as_float(fz[6]) * (-0.00005925656953246f) + as_float(fz[7]) * ( 0.00599194105227480f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01406655402962530f)  + as_float(fz[6]) * ( 1.01209554728592000f) + as_float(fz[7]) * ( 0.05507201130589120f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00376903406115139f)  + as_float(fz[6]) * ( 0.00317111328884853f) + as_float(fz[7]) * ( 1.00264983176647000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //5G(17F0114//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.98983715589376600f)  + as_float(fz[6]) * (-0.01357560019237150f) + as_float(fz[7]) * (-0.02653172967346580f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01049616415703060f)  + as_float(fz[6]) * ( 1.00813878227775000f) + as_float(fz[7]) * (-0.00159263620367056f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00915542976726635f)  + as_float(fz[6]) * (-0.00273957148966738f) + as_float(fz[7]) * ( 0.99430083848025500f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(16H0008//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00711105917983000f)  + as_float(fz[6]) * (-0.01451447413416350f) + as_float(fz[7]) * (-0.00177092505561319f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.02110388861321270f)  + as_float(fz[6]) * ( 0.99874627313776200f) + as_float(fz[7]) * (-0.00348522520212035f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00656061327099231f)  + as_float(fz[6]) * ( 0.00061835230051001f) + as_float(fz[7]) * ( 0.99263250821694200f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(16H0010//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99244545000008700f)  + as_float(fz[6]) * (-0.03095060202979470f) + as_float(fz[7]) * (-0.01351266244888540f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * (0.00647271703541696f)  + as_float(fz[6]) * ( 0.98652878505574600f) + as_float(fz[7]) * ( -0.01438915035503580f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00421643319093956f)  + as_float(fz[6]) * ( -0.00105807668824597f) + as_float(fz[7]) * ( 0.99678442888317500f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //デモ機No1　16G(17F0003//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00527648983457000f)  + as_float(fz[6]) * (-0.01772109589073550f) + as_float(fz[7]) * (-0.01651475534646740f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * (-0.00384562295709008f)  + as_float(fz[6]) * ( 1.01305120067025000f) + as_float(fz[7]) * ( 0.00578714448075537f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00120224895706720f)  + as_float(fz[6]) * ( 0.01823460363956620f) + as_float(fz[7]) * ( 1.00941201321592000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];


#elif 0 //16G(17F0102//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99436067137529600f)  + as_float(fz[6]) * (-0.01103236189176390f) + as_float(fz[7]) * (-0.01107615849219060f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00809392569208308f)  + as_float(fz[6]) * ( 0.99211773384856900f) + as_float(fz[7]) * ( 0.01437190770943420f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00167110092839671f)  + as_float(fz[6]) * (-0.00094626957219948f) + as_float(fz[7]) * ( 1.00003484139071000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(17F0117//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00413583259845000f)  + as_float(fz[6]) * (-0.02013529337927570f) + as_float(fz[7]) * (-0.01489179802118800f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01267845921548180f)  + as_float(fz[6]) * ( 1.00702431997937000f) + as_float(fz[7]) * (-0.01519305529161110f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00639652686314853f)  + as_float(fz[6]) * ( 0.01201213397972660f) + as_float(fz[7]) * ( 1.01060241099556000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(17F0118//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00059927475727000f)  + as_float(fz[6]) * (-0.01656619638448540f) + as_float(fz[7]) * (-0.01971768815904050f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01499404426050450f)  + as_float(fz[6]) * ( 1.00650702731599000f) + as_float(fz[7]) * ( 0.00155696329702890f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00001367574929849f)  + as_float(fz[6]) * (-0.00854347313611227f) + as_float(fz[7]) * ( 1.00638599947022000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(17F0119//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99875717259890000f)  + as_float(fz[6]) * (-0.01934125281532460f) + as_float(fz[7]) * (-0.04576696564924920f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.03193009939412110f)  + as_float(fz[6]) * ( 0.99978692147210900f) + as_float(fz[7]) * (-0.05631643997921820f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00291247839137182f)  + as_float(fz[6]) * ( 0.01873528647914400f) + as_float(fz[7]) * ( 1.00320005916670000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(17F0120//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.98524290852561300f)  + as_float(fz[6]) * (-0.01458075766942640f) + as_float(fz[7]) * (-0.00918143070624900f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00545489918786202f)  + as_float(fz[6]) * ( 1.00572773597095000f) + as_float(fz[7]) * (-0.00975958385203177f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.01597392767117640f)  + as_float(fz[6]) * (-0.00265417405624069f) + as_float(fz[7]) * ( 0.99422041310792600f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(17F0121//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00193416832888000f)  + as_float(fz[6]) * (-0.03049847454107710f) + as_float(fz[7]) * (-0.05018370355235880f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00927755918978049f)  + as_float(fz[6]) * ( 0.98846969626100200f) + as_float(fz[7]) * ( 0.03760617921209480f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.01178860173684720f)  + as_float(fz[6]) * (-0.00043076594631241f) + as_float(fz[7]) * ( 1.00358138774139000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //16G(17F0123//)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.98816036817668800f)  + as_float(fz[6]) * (-0.02745200463293580f) + as_float(fz[7]) * (-0.01094989080857250f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.02100198892834940f)  + as_float(fz[6]) * ( 1.01178551051211000f) + as_float(fz[7]) * (-0.01482654719549660f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.02163924465432150f)  + as_float(fz[6]) * (-0.00863014385269456f) + as_float(fz[7]) * ( 1.00026877288474000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //200G(17F0126//)
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.03944014475222000f)  + as_float(fz[6]) * (-0.01271915841178220f) + as_float(fz[7]) * (-0.00091829519163449f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00809462295858091f)  + as_float(fz[6]) * ( 0.96199093787887500f) + as_float(fz[7]) * (-0.00748366444391679f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00223917131983622f)  + as_float(fz[6]) * (-0.00053803529370725f) + as_float(fz[7]) * ( 0.98621797940539600f) - (0.0f));
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

#elif 1 //200G(17F0143//)
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.04681261819420000f)  + as_float(fz[6]) * (-0.01324893499851540f) + as_float(fz[7]) * ( 0.00204197493349797f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00319678009464231f)  + as_float(fz[6]) * ( 0.95870353585774300f) + as_float(fz[7]) * (-0.00284663731305457f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00195271338266834f)  + as_float(fz[6]) * ( 0.00089724486764650f) + as_float(fz[7]) * ( 1.01728494806241000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

#elif 0 //帝人set2(17F0116/1C1602514D/6521050129)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00816251132729000f)  + as_float(fz[6]) * (-0.0246217288762734f) + as_float(fz[7]) * (-0.0317648045868797f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.02102745535495240f)  + as_float(fz[6]) * ( 0.992589648532847f) + as_float(fz[7]) * ( 0.0291596918845460f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00753264063034406f)  + as_float(fz[6]) * ( 0.00129567929692541f) + as_float(fz[7]) * (1.00118439616703f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0109/1B1602514D/5321050136)5G
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99742097110931300f)  + as_float(fz[6]) * (-0.04314440914433870f) + as_float(fz[7]) * (-0.06076306541732600f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01424196578993120f)  + as_float(fz[6]) * ( 1.00862054275848000f) + as_float(fz[7]) * (-0.06138162041476620f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00555124114294484f)  + as_float(fz[6]) * ( 0.00460657989447861f) + as_float(fz[7]) * ( 1.00250814165217000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0115/1B1602514D/5321050137)5G
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00631526681345000f)  + as_float(fz[6]) * (-0.02195589199904970f) + as_float(fz[7]) * (-0.03672890214552030f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01119837548142090f)  + as_float(fz[6]) * ( 1.00275540554943000f) + as_float(fz[7]) * (-0.00745446545548188f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.01299908909184510f)  + as_float(fz[6]) * (-0.00104661351666726f) + as_float(fz[7]) * ( 1.01223820039518000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17C0011/1B1602514D/6521050126)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00288970642863000f)  + as_float(fz[6]) * (-0.03235846784141840f) + as_float(fz[7]) * (-0.01104905587557160f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01405311026953780f)  + as_float(fz[6]) * ( 1.00940051678551000f) + as_float(fz[7]) * (-0.02097446700603220f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00403288566932466f)  + as_float(fz[6]) * (-0.00023640619531776f) + as_float(fz[7]) * ( 1.00218373047671000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0129/1C1602514D/6521050132)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99230112016706000f)  + as_float(fz[6]) * (-0.00993521033968373f) + as_float(fz[7]) * (-0.03026140088703330f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00867420091782477f)  + as_float(fz[6]) * ( 0.98497912411858500f) + as_float(fz[7]) * (-0.01809648587479300f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00170816878269016f)  + as_float(fz[6]) * ( 0.00436466289036557f) + as_float(fz[7]) * ( 0.99222617933555200f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0132/1C1602514D/6521050135)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99336729891120000f)  + as_float(fz[6]) * (-0.02529356351166400f) + as_float(fz[7]) * ( 0.02017990128582870f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00788191118238825f)  + as_float(fz[6]) * ( 0.99692847445464900f) + as_float(fz[7]) * ( 0.01626138796841350f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00103303515400146f)  + as_float(fz[6]) * (-0.00086168365300340f) + as_float(fz[7]) * ( 0.98699895591107500f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0131/1C1602514D/6521050134)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00868881006152000f)  + as_float(fz[6]) * (-0.02700610205220640f) + as_float(fz[7]) * (-0.0266576473798772f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00744541050858227f)  + as_float(fz[6]) * ( 1.00798842364924000f) + as_float(fz[7]) * ( 0.01566340040821360f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.01155428901931950f)  + as_float(fz[6]) * ( 0.00339764395800384f) + as_float(fz[7]) * ( 1.00665589373294000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0128/1C1602514D/6521050131)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00364910503552000f)  + as_float(fz[6]) * (-0.01885371759745870f) + as_float(fz[7]) * (-0.06055372346445510f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00237762776267500f)  + as_float(fz[6]) * ( 1.01146918761380000f) + as_float(fz[7]) * ( 0.00116897674424489f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00991075847754865f)  + as_float(fz[6]) * (-0.00015419251661592f) + as_float(fz[7]) * ( 0.99769768274811300f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0092/1B1602514D/6521050127)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.01099148046229000f)  + as_float(fz[6]) * (-0.02305651382127060f) + as_float(fz[7]) * (-0.03905623472662580f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01900308035347230f)  + as_float(fz[6]) * ( 1.00893119094638000f) + as_float(fz[7]) * ( 0.02159743722389890f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00507707250441145f)  + as_float(fz[6]) * (-0.00565585351024539f) + as_float(fz[7]) * ( 0.99565281512174900f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17C0007/1B1602514D/6521050123)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00815367726534000f)  + as_float(fz[6]) * (-0.02940663620776150f) + as_float(fz[7]) * (-0.00551458208667633f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.01057200814913500f)  + as_float(fz[6]) * ( 0.98493077198431000f) + as_float(fz[7]) * (-0.00257412876131344f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.01053938758623930f)  + as_float(fz[6]) * ( 0.00994357150795024f) + as_float(fz[7]) * ( 0.99896628572736500f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0130/1C1602514D/6521050133)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00134478824171000f)  + as_float(fz[6]) * (-0.01432170284470850f) + as_float(fz[7]) * (-0.02520671698600850f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * (-0.00574928538200615f)  + as_float(fz[6]) * ( 0.99231736326444900f) + as_float(fz[7]) * (-0.00741619446696457f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00981857533236109f)  + as_float(fz[6]) * ( 0.01468982801780760f) + as_float(fz[7]) * ( 1.01358016230014000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0125/1C1602514D/6521050130)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00349466974387000f)  + as_float(fz[6]) * ( 0.00487610679871093f) + as_float(fz[7]) * (-0.00857821030649579f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.00352768548528665f)  + as_float(fz[6]) * ( 1.00698204033854000f) + as_float(fz[7]) * ( 0.01540990864856710f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * ( 0.00252696453996115f)  + as_float(fz[6]) * (-0.01529737371207220f) + as_float(fz[7]) * ( 1.01765357105195000) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17F0110/1B1602514D/6521050128)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.00082160091869000f)  + as_float(fz[6]) * (-0.03363528005952390f) + as_float(fz[7]) * (-0.00926711059071915f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.02622112501769760f)  + as_float(fz[6]) * ( 0.98784915196684800f) + as_float(fz[7]) * (-0.00421440110976621f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00454792067071540f)  + as_float(fz[6]) * ( 0.01569213190285390f) + as_float(fz[7]) * ( 1.01022879286673000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 1 //帝人set2(17C0014//)5G/1500dps
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99405942186563600f)  + as_float(fz[6]) * (-0.02528253883480110f) + as_float(fz[7]) * ( 0.02278454459192400f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.02421692450635320f)  + as_float(fz[6]) * ( 0.99173966452006500f) + as_float(fz[7]) * (-0.00127581444030581f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00433475377567728f)  + as_float(fz[6]) * (-0.00584958619382685f) + as_float(fz[7]) * ( 1.01095211030657000f) - (0.0f));

		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];
#elif 0 //帝人set2(17C0010/1B1602514D/6521050125)
		//acceleration [g]
		g_device.data[0] =  fz[1];
		g_device.data[1] =  fz[2];
		g_device.data[2] =  fz[3];
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.99534884899360400f)  + as_float(fz[6]) * (-0.00884610466412477f) + as_float(fz[7]) * ( 0.01318662357458770f) - (0.0f));
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * (-0.01022568435313150f)  + as_float(fz[6]) * ( 0.99533875224950400f) + as_float(fz[7]) * (-0.03254075465255940f) - (0.0f));
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00630463470958558f)  + as_float(fz[6]) * ( 0.01006633114249730f) + as_float(fz[7]) * (1.00124926637714000f) - (0.0f));
		//magn [G]
		g_device.data[8] = fz[9];
		g_device.data[9] = fz[10];
		g_device.data[10] = fz[11];

#elif 0 //帝人ID5
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 1.008120666781898f)  + as_float(fz[6]) * (-0.021675368997943f) + as_float(fz[7]) * (-0.003126449070279f) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * ( 0.013669739395194f)  + as_float(fz[6]) * ( 0.984516810161457f) + as_float(fz[7]) * (-0.005271623099573f) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.002946238104757f)  + as_float(fz[6]) * ( 0.0189331829215135f) + as_float(fz[7]) * (0.998743733108750f) - (0.0f)) * FRIZZ_MATH_DEG2RAD;

		//g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
#elif 1 //帝人ID3
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) * ( 0.995007140002217f)  + as_float(fz[6]) * (-0.00841567001295653f) + as_float(fz[7]) * ( 0.0138027795759221f) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[5]) * (-0.0113489044252079f)  + as_float(fz[6]) * ( 0.994947792957294f) + as_float(fz[7]) * (-0.0329048816046134f) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[5]) * (-0.00549752598317019f)  + as_float(fz[6]) * ( 0.0149019284447123f) + as_float(fz[7]) * (1.00060727934407f) - (0.0f)) * FRIZZ_MATH_DEG2RAD;

		//g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
#elif 1// 6
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.01788037f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.04969782f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.18070020f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - ( -0.16354062f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - ( 1.17818116f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - ( 1.88298756f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));

		//g_device.data[ 8] = (fz[ 9] - as_frizz_fp(0.0f)) * as_frizz_fp(1.0f);
		//g_device.data[ 9] = (fz[10] - as_frizz_fp(0.0f)) * as_frizz_fp(1.0f);
		//g_device.data[ 10] =(fz[11] - as_frizz_fp(0.0f)) * as_frizz_fp(1.0f);


#elif 0 //default
		//acceleration [g]
		g_device.data[0] =  (fz[1] - as_frizz_fp( 0.0f));
		g_device.data[1] =  (fz[2] - as_frizz_fp( 0.0f));
		g_device.data[2] =  (fz[3] - as_frizz_fp( 0.0f));
		//gyro [rad]
		g_device.data[4] = as_frizz_fp(as_float(fz[5]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[5] = as_frizz_fp(as_float(fz[6]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		g_device.data[6] = as_frizz_fp(as_float(fz[7]) - (0.0f)) * FRIZZ_MATH_DEG2RAD;
		//magn [G]
		g_device.data[8] = frizz_div((as_frizz_fp(as_float(fz[9])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[9] = frizz_div((as_frizz_fp(as_float(fz[10])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));
		g_device.data[10] =frizz_div((as_frizz_fp(as_float(fz[11])) - as_frizz_fp(0.0f)), as_frizz_fp(1.0f));


#endif




	}


	//gpio_set_data( GPIO_NO_3, 0 );

	g_device.f_need = 0;
	return result;
#endif
}

static unsigned int condition( void )
{
	unsigned int	result = 0, res_cond;

	result = get_device_condition( g_device.id );
	if( g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] != 0 ) {
		res_cond = ( *g_pDevIF->extra_function[INDEX_GET_DEVICE_CONDITION] )( 0 );
		if( ( D_RAW_DEVICE_ERR_READ & res_cond ) != 0 ) {
			set_device_condition_phyerr( g_device.id );
		} else {
			reset_device_condition_phyerr( g_device.id );
		}
	}
	return result;
}

sensor_if_t* DEF_INIT( accl_raw )( void )
																																{
	// ID
	g_device.id = ACCEL_RAW_ID;

	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );

	// init hardware
	if( ( g_pDevIF = basedevice_init( &g_devif[0], NELEMENT( g_devif ), g_device.id ) ) == 0 ) {
		return 0;
	}

	if( g_pDevIF->get_name != 0 ) {
		g_accel_name = ( *g_pDevIF->get_name )();
	}

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.get.condition = condition;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	// param
	g_device.f_active = 0;
	g_device.tick = 1;
	g_device.f_need = 0;
	g_device.ts = 0;
	g_device.remain_total = 0;
	// data
	//g_device.data = as_frizz_fp( 0.0f );

	return &( g_device.pif );
																																}

