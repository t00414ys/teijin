/*!******************************************************************************
 * @file    gyro_driver.h
 * @brief   sample program for gyro sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GYRO_DRIVER_H__
#define __GYRO_DRVIER_H__

#include "config.h"
#include "base_driver.h"



#if defined(USE_MPUXXXX)
#include "mpuxxxx_api.h"
#if	!defined(MAP_MPUXXXX)
#define	MAP_MPUXXXX		(0)
#endif
#define	MPUXXXX_DATA	{	mpuxxxx_init,					mpuxxxx_ctrl_gyro,		mpuxxxx_rcv_gyro ,		mpuxxxx_conv_gyro, 	\
							mpuxxxx_setparam_gyro,			mpuxxxx_get_ver,		mpuxxxx_get_name ,		MAP_MPUXXXX,		\
							{mpuxxxx_gyro_get_condition,	0,						0,						0}					},
#else
#define	MPUXXXX_DATA
#endif

#if defined(USE_BMI160)
#include "bmi160_api.h"
#if	!defined(MAP_BMI160)
#define	MAP_BMI160		(0)
#endif
#define	BMI160_DATA		{	bmi160_init,					bmi160_ctrl_gyro,		bmi160_rcv_gyro ,		bmi160_conv_gyro, 	\
							bmi160_setparam_gyro,			bmi160_get_ver,			bmi160_get_name ,		MAP_BMI160,			\
							{bmi160_gyro_get_condition,		0,						0,						0}					},
#else
#define	BMI160_DATA
#endif

#if defined(USE_LSM330)
#include "lsm330_api.h"
#if	!defined(MAP_LSM330)
#define	MAP_LSM330		(0)
#endif
#define	LSM330_DATA		{	lsm330_init,					lsm330_ctrl_gyro,		lsm330_rcv_gyro ,		lsm330_conv_gyro, 	\
							lsm330_setparam_gyro,			lsm330_get_ver,			lsm330_get_name ,		MAP_LSM330,			\
							{lsm330_gyro_get_condition,		0,						0,						0}					},
#else
#define	LSM330_DATA
#endif

#if defined(USE_LSM6DS3)
#include "lsm6ds3_api.h"
#if	!defined(MAP_LSM6DS3)
#define	MAP_LSM6DS3		(0)
#endif
#define	LSM6DS3_DATA		{	lsm6ds3_init,					lsm6ds3_ctrl_gyro,		lsm6ds3_rcv_gyro ,		lsm6ds3_conv_gyro, 	\
							lsm6ds3_setparam_gyro,			lsm6ds3_get_ver,			lsm6ds3_get_name ,		MAP_LSM6DS3,			\
							{lsm6ds3_gyro_get_condition,		0,						0,						0}					},
#else
#define	LSM6DS3_DATA
#endif

#if defined(USE_GYRO_EMU)
#include "gyroemu_api.h"
#if	!defined(MAP_GYROEMU)
#define	MAP_GYROEMU		(0)
#endif
#define	GYROEMU_DATA	{	gyroemu_init,					gyroemu_ctrl_gyro,		gyroemu_rcv_gyro ,		gyroemu_conv_gyro, 	\
							gyroemu_setparam_gyro,			gyroemu_get_ver,		gyroemu_get_name ,		MAP_GYROEMU,		\
							{gyroemu_get_condition,			0,						0,						0}					},
#else
#define	GYROEMU_DATA
#endif

#endif // __GYRO_DRIVER_H__
