/*!******************************************************************************
 * @file    ecg_driver.h
 * @brief   sample program for ecg sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __ECG_DRIVER_H__
#define __ECG_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_BMD101)
#include "bmd101_api.h"
#define	BMD101_DATA	{	bmd101_init,			bmd101_ctrl,		bmd101_rcv ,		bmd101_conv, 	\
						bmd101_setparam,		bmd101_get_ver,		bmd101_get_name ,	0,				\
						{bmd101_get_condition,	0,					0,					0}				},
#else
#define	BMD101_DATA
#endif

#endif /*  */
