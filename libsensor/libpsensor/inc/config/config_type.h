/*!******************************************************************************
 * @file    raw_drivers.h
 * @brief   raw driver name
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

#ifndef __CONFIG_TYPE_H__
#define __CONFIG_TYPE_H__

typedef enum {
	D_BOARD_CHIGNON2 = 1,
	D_BOARD_CHIGNONB,
	D_BOARD_AYAME,
	D_BOARD_AYAME_KURO
} BOARD_TYPE;

#define		D_DRIVER_NAME_EMUA					(0x454D5541)			// 'EMUA'
#define		D_DRIVER_NAME_EMUG					(0x454D5547)			// 'EMUG'
#define		D_DRIVER_NAME_EMUL					(0x454D554C)			// 'EMUL'
#define		D_DRIVER_NAME_EMUX					(0x454D5558)			// 'EMUX'
#define		D_DRIVER_NAME_EMUP					(0x454D5550)			// 'EMUP'
#define		D_DRIVER_NAME_EMUM					(0x454D554D)			// 'EMUM'
#define		D_DRIVER_NAME_EMPG					(0x454D5047)			// 'EMPG'





//ASCII 文字コード
#define		D_DRIVER_NAME_AK0991X				(0x414B3039)			// 'AK09'
#define		D_DRIVER_NAME_AKk8963				(0x414B3839)			// 'AK89'
#define		D_DRIVER_NAME_BMA2XX				(0x424D4132)			// 'BMA2'
#define		D_DRIVER_NAME_BMI160				(0x424D4936)			// 'BMI6'
#define		D_DRIVER_NAME_BMM150				(0x424D4D35)			// 'BMM5'
#define		D_DRIVER_NAME_BMP280				(0x424D5038)			// 'BMP8'
#define		D_DRIVER_NAME_GP2AP054				(0x47503241)			// 'GP2A'
#define		D_DRIVER_NAME_HSPPAD031				(0x48535050)			// 'HSPP'
#define		D_DRIVER_NAME_LPS25H				(0x4C505348)			// 'LPSH'
#define		D_DRIVER_NAME_LSM330				(0x4C534D33)			// 'LSM3'
#define		D_DRIVER_NAME_LT1PH03				(0x4C543150)			// 'LT1P'
#define		D_DRIVER_NAME_PAH8001				(0x50414838)			// 'PAH8'
#define		D_DRIVER_NAME_ADXL362				(0x41333632)			// 'A362'
#define		D_DRIVER_NAME_ADS8332				(0x41383332)			// 'A832'
#define		D_DRIVER_NAME_HMC5883				(0x48353838)			// 'H588'
#define		D_DRIVER_NAME_MC3413				(0x4D433334)			// 'MC34'
#define		D_DRIVER_NAME_MMC3516				(0x4D4D4336)			// 'MMC6'
#define		D_DRIVER_NAME_MPL115A2				(0x4D504C32)			// 'MPL2'
#define		D_DRIVER_NAME_MPUXXXX				(0x4D505558)			// 'MPUX'
#define		D_DRIVER_NAME_SSD1306				(0x53534436)			// 'SSD6'
#define		D_DRIVER_NAME_STK3420				(0x53543334)			// 'ST34'
#define		D_DRIVER_NAME_ZPA2326				(0x5A504136)			// 'ZPA6'
#define		D_DRIVER_NAME_RAWMOCK				(0x4D4F434B)			// 'MOCK'
#define		D_DRIVER_NAME_BME280				(0x424D4538)			// 'BME8'
#define		D_DRIVER_NAME_STK8313				(0x53543833)			// 'ST83'
#define		D_DRIVER_NAME_PM131					(0x504D3133)			// 'PM13'
#define		D_DRIVER_NAME_PAC7673EE				(0x50413736)			// 'PA76'
#define		D_DRIVER_NAME_PA12201001			(0x50413132)			// 'PA12'
#define		D_DRIVER_NAME_LSM6DS3				(0x4C534D36)			// 'LSM6'
#define		D_DRIVER_NAME_LIS2DS12				(0x4C495332)			// 'LIS2'
#define		D_DRIVER_NAME_LIS2DH				(0x4C533244)			// 'LS2D'
#define		D_DRIVER_NAME_HY3118				(0x48593338)			// 'HY38'
#define		D_DRIVER_NAME_HTS221				(0x48545332)			// 'HTS2'
#define		D_DRIVER_NAME_GTAP200				(0x47544132)			// 'GTA2'
#define		D_DRIVER_NAME_BMD101				(0x424D4431)			// 'BMD1'
#define		D_DRIVER_NAME_ADPD					(0x41445044)			// 'ADPD'
#define		D_DRIVER_NAME_HP203B				(0x48503242)			// 'HP2B'
#define    D_DRIVER_NAME_MAG3110               (0x4D414733)            // 'MAG3' MAG3110




#endif /*__CONFIG_TYPE_H__  */
