/*!******************************************************************************
 * @file    magn_param.h
 * @brief   virtual magnet parameter sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "mag_util.h"
#include "sensor_if.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "libsensors_id.h"
#include "magn_param.h"
#include "if/magn_param_if.h"

#define		SENSOR_VER_MAJOR		(1)						// Major Version
#define		SENSOR_VER_MINOR		(0)						// Minor Version
#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

#define MAGNET_PARAMETER_TICK	20	// o:50

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[1];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_magn;
	// status
	int					f_active;
	int					f_need;
	unsigned int		ts;
#ifdef USE_MCC_MAG_CALIBRATION
	unsigned int		count;
#endif
	// data
	magnet_parameter_data_t	data;
} device_sensor_t;

static device_sensor_t g_device;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return sizeof( g_device.data ) / sizeof( int );
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == SENSOR_ID_MAGNET_RAW ) {
		g_device.p_magn = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_interval( g_device.id, g_device.p_magn->id(), MAGNET_PARAMETER_TICK );
		hub_mgr_set_sensor_active( g_device.id, g_device.p_magn->id(), g_device.f_active );
#ifdef USE_MCC_MAG_CALIBRATION
		if( f_active != 0 ) {
			if( g_device.data.f_calib == 0 ) {
				MagUtil_Init();
				g_device.count = 0;
			}
		}
#endif
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	return 0;
}

static int get_interval( void )
{
	return 0;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.ts = ts;
#ifdef USE_MCC_MAG_CALIBRATION
	if( g_device.data.f_calib == 0 ) {
		g_device.f_need = 1;
	}
#endif
	return ts + MAGNET_PARAMETER_TICK;
}

static int calculate( void )
{
	int ret = 1;
#ifdef USE_MCC_MAG_CALIBRATION
	if( g_device.count < ( 10000 / MAGNET_PARAMETER_TICK ) ) {
		frizz_fp *p;
		g_device.p_magn->data( ( void** )&p, &g_device.ts );
		MagUtil_CalibPush( p );
		g_device.count++;
		if( g_device.count == ( 10000 / MAGNET_PARAMETER_TICK ) ) {
			MagUtil_CalibCalc();
			MagUtil_GetSoftComp( &( g_device.data.param[0] ) );
			MagUtil_GetHardComp( &( g_device.data.param[9] ) );
			g_device.data.f_calib = 1;
		} else {
			ret = 0;
		}
	}
#endif
	g_device.f_need = 0;
	return ret;
}

static int command( unsigned int cmd, void *param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case MAGNET_PARAMETER_CMD_GET_STATUS:
		ret = g_device.data.f_calib;
		break;
	case MAGNET_PARAMETER_CMD_SET_PARAMETER: {
			frizz_fp *p = ( frizz_fp* )param;
			int i;
#ifdef USE_MCC_MAG_CALIBRATION
			MagUtil_SetSoftComp( &( p[0] ) );
			MagUtil_SetHardComp( &( p[9] ) );
#endif
			for( i = 0; i < 12; i++ ) {
				g_device.data.param[i] = p[i];
			}
			g_device.data.f_calib = 1;
			g_device.f_need = 1;
		}
		ret = 0;
		break;
	case SENSOR_GET_VERSION:
		ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		break;
	default:
		ret = -1;
	}
	return ret;
}

#ifdef USE_MCC_MAG_CALIBRATION
static void end( void )
{
	MagUtil_Destroy();
}
#endif

sensor_if_t* DEF_INIT( magn_param )( void )
{
	// ID
	g_device.id = MAGNET_PARAMETER_ID;
	g_device.par_ls[0] = SENSOR_ID_MAGNET_RAW;
	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
#ifdef USE_MCC_MAG_CALIBRATION
	g_device.pif.end = end;
#else
	g_device.pif.end = 0;
#endif
	// param
	g_device.f_active = 0;
	g_device.f_need = 0;
#ifdef USE_MCC_MAG_CALIBRATION
	// Init
	MagUtil_Create();
#endif
	// Initilize
	g_device.data.param[1] =
		g_device.data.param[2] =
			g_device.data.param[3] =
				g_device.data.param[5] =
					g_device.data.param[6] =
						g_device.data.param[7] =
							g_device.data.param[9] =
								g_device.data.param[10] =
									g_device.data.param[11] = as_frizz_fp( 0.0f );
	g_device.data.param[0] =
		g_device.data.param[4] =
			g_device.data.param[8] = as_frizz_fp( 1.0f );
	g_device.data.f_calib = 0;

	return &( g_device.pif );
}

#ifndef USE_MCC_MAG_CALIBRATION
/**
 * @brief set calibration status
 *
 * @param f_calib 0:uncalibrated, 1:calibrated
 */
void magn_param_set_calibration_status( int f_calib )
{
	if( g_device.data.f_calib != f_calib ) {
		g_device.data.f_calib = f_calib;
		g_device.f_need = 1;
	}
}

/**
 * @brief set calibration parameter
 *
 * @param soft[9] soft iron parameter(compensation 3x3 matrix)
 * @param hard[3] hard iron parameter(offset)
 *
 * @note if NULL pointer set, it will not use.
 */
void magn_param_set_calibration_parameter( frizz_fp *soft, frizz_fp *hard )
{
	int i;
	if( soft ) {
		for( i = 0; i < 9; i++ ) {
			g_device.data.param[i] = soft[i];
		}
	}
	if( soft ) {
		for( i = 0; i < 3; i++ ) {
			g_device.data.param[9 + i] = hard[i];
		}
	}
	g_device.f_need = 1;
}
#endif
