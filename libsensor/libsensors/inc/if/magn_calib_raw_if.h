/*!******************************************************************************
 * @file    magn_calib_raw_if.h
 * @brief   virtual magnet calibration raw sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAGN_CALIB_RAW_IF_H__
#define __MAGN_CALIB_RAW_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup MAGNET_CALIB_RAW MAGNET CALIB RAW
 *  @{
 */
#define MAGNET_CALIB_RAW_ID	SENSOR_ID_MAGNET_CALIB_RAW //!< RAW-Iron calibration I/F

/**
 * @struct magnet_calib_raw_result_t
 * @brief 	cagnet_Output data structure for magnet calibration raw sensor
 */
typedef struct {
	unsigned char				bDoneInit;		//!< calibration was conducted
	//   * = 0, Not Init
	//   * = 1, Init Done
	unsigned char				quality;		//!< calibration quality
	//   * = 0, quality = 0%.
	//   * = 3, quality = 100%.
	unsigned char				bMagnvalue;		//!< Magnetic data is valid
	//   * = 0, OK
	//   * = 1, NG
	unsigned char				bCount;			//!< calibration count
} magnet_calib_raw_result_t;


/**
 * @struct magnet_calib_raw_data_t
 * @brief 	Output data structure for magnet calibration raw sensor
 */
typedef struct {
	int							size;			//!< calibration data size
	unsigned char				result[64];		//!< calibration data
	//  * [akm8963 save data = 24byte]
	//  * [mmc3516 save data = 44byte]
	magnet_calib_raw_result_t	status;			//!< calibration result
} magnet_calib_raw_data_t;

/**
 * @name Command List
 */
//@{
/**
 * @brief  Get calibration Status
 * @param  none
 * @return (magnet_calib_raw_result_t *)
 */
#define MAGNET_CALIB_RAW_GET_STATUS		(0x00)

/**
 * @brief  Set calibration data
 * @param  magnet_calib_raw_data_t
 * @return (magnet_calib_raw_data_t *)
 */
#define MAGNET_CALIB_RAW_SET_CALIBDATA	(0x01)

/**
 * @brief  Get calibration data
 * @param  magnet_calib_raw_data_t
 * @return (magnet_calib_raw_data_t *)
 */
#define MAGNET_CALIB_RAW_GET_CALIBDATA	(0x02)
//@}


/** @} */
#endif
