/*!******************************************************************************
 * @file    accl_line_if.h
 * @brief   virtual accel linear interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCL_LINE_IF_H__
#define __ACCL_LINE_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup ACCEL_LINEAR ACCEL LINEAR SENSOR
 *  @{
 */

#define ACCEL_LINE_ID	SENSOR_ID_ACCEL_LINEAR	//!< A linear accelerometer ID

/**	@struct accel_line_data_t
 *	@brief Output data structure for accel linear sensor
 */
typedef struct {
	FLOAT		data[3];	//!< data[0]:X-axis Acceleration[G], data[1]:Y-axis Acceleration[G], data[2]:Z-axis Acceleration[G]
} accel_line_data_t;

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
