/*!******************************************************************************
 * @file    wrist_tilt_det_if.h
 * @brief   virtual wrist tilt detector if sensor interface
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __WRIST_TILT_DET_IF_H__
#define __WRIST_TILT_DET_IF_H__

#include "libsensors_id.h"

/** @defgroup WRIST_TILT_DET WRIST_TILT DETECTOR
 *  @{
 */
#define WRIST_TILT_DET_ID SENSOR_ID_WRIST_TILT_DETECTOR	//!< An wrist tilt detector sensor interface ID

/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
