/*!******************************************************************************
 * @file    prs_stair_if.h
 * @brief   virtual stair detector sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PRS_STAIR_IF_H__
#define __PRS_STAIR_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup STAIR_DETECTOR STAIR DETECTOR
 *  @{
 */

#define STAIR_DETECTOR_ID	SENSOR_ID_STAIR_DETECTOR //!< Stair detector using pressure sensor

/**
 * @struct prs_stair_data_t
 * @brief Output data structure for stair detector sensor
 * @brief walk state + altitude
 */
typedef struct {
	int		state;		//!< 0: walk up stair, 2: walk floor, 4: walk down stair
	FLOAT	altitude;	//!< relative altitude from the floor of device startup
} prs_stair_data_t;
/**
 * @name Command List
 * @note none
 */
//@{
//@}
/** @} */
#endif
