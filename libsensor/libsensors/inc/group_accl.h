/*!******************************************************************************
 * @file    group_accl.h
 * @brief   group of virtual accel sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/**
 *  @brief accel header group
 */
#ifndef __GROUP_ACCELEROMETER_H__
#define __GROUP_ACCELEROMETER_H__

#include "accl_hpf.h"
#include "accl_lpf.h"
#include "accl_pow.h"
#include "accl_line.h"
#include "accl_stat.h"

#endif
