/*!******************************************************************************
 * @file    mpuxxxx.h
 * @brief   mpuxxxx sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MPUXXXX_H__
#define __MPUXXXX_H__
#include "mpuxxxx_api.h"

/*
 *
 *		By environment, you must edit this file
 *
 */

/*
 * This is use accel sensor range (To select either one)
 *(DEFINITED) : Enable  use accl sensor range
 *(~DEFINITED): Disable  use accl sensor range
 */
//#define		ACC_2G_RANGE
//#define		ACC_4G_RANGE
//#define		ACC_8G_RANGE
#define		ACC_16G_RANGE






/*
 *
 *		The rest of this is okay not change
 *
 */
#define MPUXXXX_DEG2RAD					0.01745329251994329576923690768489

// FS_SEL=3: +- 1000 (degree/s)
// 32768/1000 LSB/(deg/s) => 1000/32768 (deg/s)/LSB => deg2rad*1000/32768 (rad/s)/LSB
#define MPUXXXX_GYRO_PER_LSB		((float)(MPUXXXX_DEG2RAD*1000./32768.))
// 333.87 (LSB/C) => 1/333.87 (C/LSB)
#define MPUXXXX_TEMPERATURE_PER_LSB		((float)(1./333.87))

#define MPUXXXX_FULLCHIP_SLEEP			1
#define MPUXXXX_ACCL_LOWPOWER			1

#define MPUXXXX_I2C_ADDRESS_H			0x69
#define MPUXXXX_I2C_ADDRESS_L			0x68

#define MPUXXXX_WHOAMI_ID_MPU6505_6500	0x70
#define MPUXXXX_WHOAMI_ID_MPU9255		0x73
#define MPUXXXX_WHOAMI_ID_MPU9250		0x71

/*  register map*/
#define MPUXXXX_SMPLRT_DIV			0x19
#define MPUXXXX_CONFIG				0x1A
#define MPUXXXX_GYRO_CONFIG			0x1B
#define MPUXXXX_ACCEL_CONFIG		0x1C
#define MPUXXXX_ACCEL_CONFIG2		0x1D
#define MPUXXXX_LP_ACCEL_ODR		0x1E
#define MPUXXXX_WOM_THR				0x1F

#define MPUXXXX_FIFO_EN				0x23
#define MPUXXXX_I2C_MST_CTRL		0x24
#define MPUXXXX_I2C_SLV0_ADDR		0x25
#define MPUXXXX_I2C_SLV0_REG		0x26
#define MPUXXXX_I2C_SLV0_CTRL		0x27
#define MPUXXXX_I2C_SLV1_ADDR		0x28
#define MPUXXXX_I2C_SLV1_REG		0x29
#define MPUXXXX_I2C_SLV1_CTRL		0x2A
#define MPUXXXX_I2C_SLV2_ADDR		0x2B
#define MPUXXXX_I2C_SLV2_REG		0x2C
#define MPUXXXX_I2C_SLV2_CTRL		0x2D
#define MPUXXXX_I2C_SLV3_ADDR		0x2E
#define MPUXXXX_I2C_SLV3_REG		0x2F
#define MPUXXXX_I2C_SLV3_CTRL		0x30
#define MPUXXXX_I2C_SLV4_ADDR		0x31
#define MPUXXXX_I2C_SLV4_REG		0x32
#define MPUXXXX_I2C_SLV4_DO			0x33
#define MPUXXXX_I2C_SLV4_CTRL		0x34
#define MPUXXXX_I2C_SLV4_DI			0x35
#define MPUXXXX_I2C_MST_STATUS		0x36
#define MPUXXXX_INT_PIN_CFG			0x37
#define MPUXXXX_INT_ENABLE			0x38

#define MPUXXXX_INT_STATUS			0x3A
#define MPUXXXX_ACCEL_XOUT_H		0x3B
#define MPUXXXX_ACCEL_XOUT_L		0x3C
#define MPUXXXX_ACCEL_YOUT_H		0x3D
#define MPUXXXX_ACCEL_YOUT_L		0x3E
#define MPUXXXX_ACCEL_ZOUT_H		0x3F
#define MPUXXXX_ACCEL_ZOUT_L		0x40
#define MPUXXXX_TEMP_OUT_H			0x41
#define MPUXXXX_TEMP_OUT_L			0x42
#define MPUXXXX_GYRO_XOUT_H			0x43
#define MPUXXXX_GYRO_XOUT_L			0x44
#define MPUXXXX_GYRO_YOUT_H			0x45
#define MPUXXXX_GYRO_YOUT_L			0x46
#define MPUXXXX_GYRO_ZOUT_H			0x47
#define MPUXXXX_GYRO_ZOUT_L			0x48
#define MPUXXXX_EXT_SENS_DATA_00	0x49
#define MPUXXXX_EXT_SENS_DATA_01	0x4A
#define MPUXXXX_EXT_SENS_DATA_02	0x4B
#define MPUXXXX_EXT_SENS_DATA_03	0x4C
#define MPUXXXX_EXT_SENS_DATA_04	0x4D
#define MPUXXXX_EXT_SENS_DATA_05	0x4E
#define MPUXXXX_EXT_SENS_DATA_06	0x4F
#define MPUXXXX_EXT_SENS_DATA_07	0x50
#define MPUXXXX_EXT_SENS_DATA_08	0x51
#define MPUXXXX_EXT_SENS_DATA_09	0x52
#define MPUXXXX_EXT_SENS_DATA_10	0x53
#define MPUXXXX_EXT_SENS_DATA_11	0x54
#define MPUXXXX_EXT_SENS_DATA_12	0x55
#define MPUXXXX_EXT_SENS_DATA_13	0x56
#define MPUXXXX_EXT_SENS_DATA_14	0x57
#define MPUXXXX_EXT_SENS_DATA_15	0x58
#define MPUXXXX_EXT_SENS_DATA_16	0x59
#define MPUXXXX_EXT_SENS_DATA_17	0x5A
#define MPUXXXX_EXT_SENS_DATA_18	0x5B
#define MPUXXXX_EXT_SENS_DATA_19	0x5C
#define MPUXXXX_EXT_SENS_DATA_20	0x5D
#define MPUXXXX_EXT_SENS_DATA_21	0x5E
#define MPUXXXX_EXT_SENS_DATA_22	0x5F
#define MPUXXXX_EXT_SENS_DATA_23	0x60

#define MPUXXXX_I2C_SLV0_DO			0x63
#define MPUXXXX_I2C_SLV1_DO			0x64
#define MPUXXXX_I2C_SLV2_DO			0x65
#define MPUXXXX_I2C_SLV3_DO			0x66
#define MPUXXXX_I2C_MST_DELAY_CTRL	0x67
#define MPUXXXX_SIGNAL_PATH_RESET	0x68
#define MPUXXXX_ACCEL_INTEL_CTRL	0x69
#define MPUXXXX_USER_CTRL			0x6A
#define MPUXXXX_PWR_MGMT_1			0x6B
#define MPUXXXX_PWR_MGMT_2			0x6C

#define MPUXXXX_FIFO_COUNT_H		0x72
#define MPUXXXX_FIFO_COUNT_L		0x73
#define MPUXXXX_FIFO_R_W			0x74
#define MPUXXXX_WHO_AM_I			0x75

#define MPUXXXX_XA_OFFSET_H			0x77
#define MPUXXXX_XA_OFFSET_L			0x78

#define MPUXXXX_YA_OFFSET_H			0x7A
#define MPUXXXX_YA_OFFSET_L			0x7B

#define MPUXXXX_ZA_OFFSET_H			0x7D
#define MPUXXXX_ZA_OFFSET_L			0x7E


/*  constant*/
#define	MPUXXXX_ACC_2G		(0x0<<3)
#define	MPUXXXX_ACC_4G		(0x1<<3)
#define	MPUXXXX_ACC_8G		(0x2<<3)
#define	MPUXXXX_ACC_16G		(0x3<<3)

#if defined(ACC_2G_RANGE)
#define MPUXXXX_ACC_RANGE			MPUXXXX_ACC_2G
#define MPUXXXX_ACCEL_PER_LSB		(1./16384.)
#endif
#if defined(ACC_4G_RANGE)
#define MPUXXXX_ACC_RANGE			MPUXXXX_ACC_4G
#define MPUXXXX_ACCEL_PER_LSB		(1./8192.)
#endif
#if defined(ACC_8G_RANGE)
#define MPUXXXX_ACC_RANGE			MPUXXXX_ACC_8G
#define MPUXXXX_ACCEL_PER_LSB		(1./4096.)
#endif
#if defined(ACC_16G_RANGE)
#define MPUXXXX_ACC_RANGE			MPUXXXX_ACC_16G
#define MPUXXXX_ACCEL_PER_LSB		(1./2048.)
#endif

#if defined(MPUXXXX_FULLCHIP_SLEEP)
typedef struct {
	int		data;
} mpuxxxx_ctrl_t;

extern mpuxxxx_ctrl_t mpuxxxx_ctrl;
extern void mpuxxxx_set_chip_wake( void );
extern void mpuxxxx_set_chip_sleep( void );
#endif

#endif
