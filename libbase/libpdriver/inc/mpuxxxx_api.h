/*!******************************************************************************
 * @file    mpuxxxx_api.h
 * @brief   mpuxxxx sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MPUXXXX_API_H__
#define __MPUXXXX_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int mpuxxxx_init( unsigned int param );
void mpuxxxx_ctrl_accl( int f_ena );
void mpuxxxx_ctrl_gyro( int f_ena );
unsigned int mpuxxxx_rcv_accl( unsigned int tick );
int mpuxxxx_conv_accl( frizz_fp data[3] );
unsigned int mpuxxxx_rcv_gyro( unsigned int tick );
int mpuxxxx_conv_gyro( frizz_fp data[4] );

int mpuxxxx_gyro_get_condition( void *data );

int mpuxxxx_setparam_accl( void *ptr );
int mpuxxxx_setparam_gyro( void *ptr );
unsigned int mpuxxxx_get_ver( void );
unsigned int mpuxxxx_get_name( void );
unsigned char mpuxxxx_get_id( void );
unsigned char mpuxxxx_get_addr( void );

int mpuxxxx_accl_get_condition( void *data );
int mpuxxxx_accl_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif


#endif
