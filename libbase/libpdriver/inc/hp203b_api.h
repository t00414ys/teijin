/*!******************************************************************************
 * @file    hp203b_api.h
 * @brief   hp203b sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PRESS_HP203B_API_H__
#define __PRESS_HP203B_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int hp203b_init(unsigned int param);
void hp203b_ctrl(int f_ena);
unsigned int hp203b_rcv(unsigned int tick);
int hp203b_conv(frizz_fp* press_data);

int hp203b_setparam(void *ptr);
unsigned int hp203b_get_ver(void);
unsigned int hp203b_get_name(void);

int hp203b_get_condition(void *data);


#ifdef __cplusplus
}
#endif


#endif
