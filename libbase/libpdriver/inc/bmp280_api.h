/*!******************************************************************************
 * @file    bmp280_api.h
 * @brief   bmp280 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMP280_API_H__
#define __BMP280_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif


int bmp280_init( unsigned int param );
void bmp280_ctrl( int f_ena );
unsigned int bmp280_rcv( unsigned int tick );
int bmp280_conv( frizz_fp* press_data );

int bmp280_setparam( void *ptr );
unsigned int bmp280_get_ver( void );
unsigned int bmp280_get_name( void );

int bmp280_get_condition( void *data );

#ifdef __cplusplus
}
#endif


#endif
