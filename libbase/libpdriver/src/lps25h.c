/*!******************************************************************************
 * @file    lps25h.c
 * @brief   lps25h sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "lps25h.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_LPS25H


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version


struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[5];	// transmission buffer, pres[3]+temp[2]
	unsigned char		addr;		// detect automatically
	unsigned char		device_id;
	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[1];
	//
} g_press_lps25h;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	PRES_lps25h_I2C_ADDRESS_H,
	PRES_lps25h_I2C_ADDRESS_L,
};

int lps25h_init( unsigned int param )
{
	int ret, i;
	unsigned char buff;
	unsigned char data;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	// Get Device ID
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_press_lps25h.addr =  add_tbl[i];
		g_press_lps25h.device_id = 0;			// read id
		ret = i2c_read( g_press_lps25h.addr, LPS25H_WHO_AM_I, &g_press_lps25h.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( g_press_lps25h.device_id == LPS25H_DEVICE_ID ) {
				break;
			}
		}
	}

	if( g_press_lps25h.device_id != LPS25H_DEVICE_ID ) {
		return RESULT_ERR_INIT;
	}
	// @@

	////////////////////
	//
	// read output data rate & power on-off mode
	buff = 0;
	ret = i2c_read( g_press_lps25h.addr, LPS25H_CTRL_REG1 , &buff, 1 );	// output data rate
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	buff  = buff & 0x0f;
	buff  = buff | ( LPS25H_FREQ_25HZ << LPS25H_ODR_BIT );			// init 25Hz
	buff  = buff | ( 0x01 << LPS25H_BDU );			// 1: output registers not updated until MSB and LSB have been read)
	data  = buff | ( 0x00 << LPS25H_POWER_DOWN );				// disable first.
	ret = i2c_write( g_press_lps25h.addr, LPS25H_CTRL_REG1 , &data, 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}


void lps25h_ctrl( int f_ena )
{
	unsigned char buff;

	buff = 0;
	i2c_read( g_press_lps25h.addr, LPS25H_CTRL_REG1 , &buff, 1 );	// activeMode

	buff      = buff & 0x7f;
	if( f_ena ) {
		buff = buff | ( 0x01 << LPS25H_POWER_DOWN );  	//enable Pressure sensor
	} else {
		buff = buff | ( 0x00 << LPS25H_POWER_DOWN );  	//disable Pressure sensor
	}

	i2c_write( g_press_lps25h.addr, LPS25H_CTRL_REG1 , &buff, 1 );	// activeMode
}

unsigned int lps25h_rcv( unsigned int tick )
{
	// pressure 3byte
	g_press_lps25h.recv_result = i2c_read( g_press_lps25h.addr, LPS25H_PRESS_OUT_XL | LPS25H_I2C_BURSTREAD , &g_press_lps25h.buff[0], 3 );
	if( g_press_lps25h.recv_result == 0 ) {
		g_press_lps25h.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_press_lps25h.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}

int lps25h_conv( frizz_fp* press_data )
{
	frizz_fp fz;
	float* fp = ( float* )&fz;
	unsigned int sensor_buff;

	if( g_press_lps25h.recv_result != 0 ) {
		*press_data = g_press_lps25h.lasttime_data[0];
		return RESULT_SUCCESS_CONV;
	}

	// press value MSB 18bit
	sensor_buff = ( ( ( unsigned int ) g_press_lps25h.buff[2] << 16 ) |
					( ( unsigned int ) g_press_lps25h.buff[1] <<  8 ) |
					( ( unsigned int ) g_press_lps25h.buff[0] <<  0 ) );

	fp[0] = ( float )sensor_buff;

	// Hpa
	*press_data = fz / as_frizz_fp( 4096.0 );
	//
	g_press_lps25h.lasttime_data[0] = *press_data;

	return RESULT_SUCCESS_CONV;
}


int lps25h_setparam( void *ptr )
{
	return RESULT_ERR_SET;
}


int lps25h_get_condition( void *data )
{
	return g_press_lps25h.device_condition;
}


unsigned int lps25h_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int lps25h_get_name()
{
	return	D_DRIVER_NAME;
}

