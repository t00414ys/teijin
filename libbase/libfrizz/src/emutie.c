/*!******************************************************************************
 * @file emutie.c
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "emutie.h"
#include "frizz_op.h"
#include "frizz_fp4w.h"

FRIZZ_INLINE void frizz_tie_store_const( frizz_fp num, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] = frizz_tie_store_const4w( num );
	}
}

FRIZZ_INLINE void frizz_tie_ppmov( frizz_fp4w *src, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] = src[i];
	}
}

FRIZZ_INLINE void frizz_tie_ppadd( frizz_fp4w *src0, frizz_fp4w *src1, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] = src0[i] + src1[i];
	}
}

FRIZZ_INLINE void frizz_tie_ppsub( frizz_fp4w *src0, frizz_fp4w *src1, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] = src0[i] - src1[i];
	}
}

FRIZZ_INLINE void frizz_tie_ppmul( frizz_fp4w *src0, frizz_fp4w *src1, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] = src0[i] * src1[i];
	}
}

FRIZZ_INLINE void frizz_tie_ppmul_pdadd( frizz_fp4w *src0, frizz_fp4w *src1, frizz_fp *dst, int blks )
{
	frizz_fp4w sum;
	int i = 0;
	sum = src0[i] * src1[i];
	i++;
	for( ; i < blks; i++ ) {
		sum += src0[i] * src1[i];
	}
	*dst = frizz_tie_vreduc( sum );
}

FRIZZ_INLINE void frizz_tie_spmul( frizz_fp src0, frizz_fp4w *src1, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] = src0 * src1[i];
	}
}

FRIZZ_INLINE void frizz_tie_spmul_ppadd( frizz_fp src0, frizz_fp4w *src1, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] += src0 * src1[i];
	}
}

FRIZZ_INLINE void frizz_tie_spmul_ppsub( frizz_fp src0, frizz_fp4w *src1, frizz_fp4w *dst, int blks )
{
	int i;
	for( i = 0; i < blks; i++ ) {
		dst[i] -= src0 * src1[i];
	}
}

