/*!******************************************************************************
 * @file emutie.h
 * @brief
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __EMUTIE_H__
#define __EMUTIE_H__
#include "frizz_fp4w.h"

#define FRIZZ_VALID_INLINE
#ifdef FRIZZ_VALID_INLINE
#define FRIZZ_INLINE static __inline__
#else
#define FRIZZ_INLINE
#endif

#ifdef __cplusplus
extern "C" {
#endif

FRIZZ_INLINE void frizz_tie_store_const( frizz_fp, frizz_fp4w*, int );
FRIZZ_INLINE void frizz_tie_ppmov( frizz_fp4w*, frizz_fp4w*, int );
FRIZZ_INLINE void frizz_tie_ppadd( frizz_fp4w*, frizz_fp4w*, frizz_fp4w*, int );
FRIZZ_INLINE void frizz_tie_ppsub( frizz_fp4w*, frizz_fp4w*, frizz_fp4w*, int );
FRIZZ_INLINE void frizz_tie_ppmul( frizz_fp4w*, frizz_fp4w*, frizz_fp4w*, int );
FRIZZ_INLINE void frizz_tie_ppmul_pdadd( frizz_fp4w*, frizz_fp4w*, frizz_fp*, int );

FRIZZ_INLINE void frizz_tie_spmul( frizz_fp, frizz_fp4w*, frizz_fp4w*, int );
FRIZZ_INLINE void frizz_tie_spmul_ppadd( frizz_fp, frizz_fp4w*, frizz_fp4w*, int );
FRIZZ_INLINE void frizz_tie_spmul_ppsub( frizz_fp, frizz_fp4w*, frizz_fp4w*, int );

#ifdef __cplusplus
}
#endif

#endif//__EMUTIE_H__
