/*!******************************************************************************
 * @file frizz_log.c
 * @brief frizz_log() function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief constant value for frizz_log
 */
static const float _log_c[] = {
	6.9313812256e-01,	/* 0x3f317180 */
	9.0580006145e-06,	/* 0x3717f7d1 */
	6.6666668653e-01,	/* 3F2AAAAB */
	4.0000000596e-01,	/* 3ECCCCCD */
	2.8571429849e-01,	/* 3E924925 */
	2.2222198546e-01,	/* 3E638E29 */
	1.8183572590e-01,	/* 3E3A3325 */
	1.5313838422e-01,	/* 3E1CD04F */
	1.4798198640e-01,	/* 3E178897 */
	0.33333333333333333,
};

/**
 * @brief index of constant value for frizz_log
 */
enum {
	LOG_C_ln2_hi = 0,
	LOG_C_ln2_lo,
	LOG_C_Lg1,
	LOG_C_Lg2,
	LOG_C_Lg3,
	LOG_C_Lg4,
	LOG_C_Lg5,
	LOG_C_Lg6,
	LOG_C_Lg7,
	LOG_C_333,
};

/**
 * @brief natural logarithm
 *
 * @param [in] x
 *
 * @return natural logarithm of x
 */
frizz_fp frizz_log( frizz_fp x )
{
	const frizz_fp *c = ( const frizz_fp* )_log_c;
	_MathFPCast ix, tx;
	frizz_fp hfsq, f, s, z, R, w, t1, t2, dk;
	int k, i, j;

	ix.zp = x;

	k = ix.fbit.exp;
	if( ( ix.ui & 0x7fffffff ) == 0 ) {
		ix.ui = 0xff800000;
		return ix.zp;		/* log(+-0)=-inf */
	}
	if( ix.fbit.sign != 0 ) {
		ix.ui = 0x7fc00000;
		return ix.zp;	/* log(-#) = NaN */
	}
	if( ix.ui == 0x7f800000 ) {
		return x;
	}
	if( k == 0 ) {
		ix.zp *= FRIZZ_MATH_TWO25;
		k = -25;
	}
	k -= 127;
	ix.ui = ix.fbit.frac;
	i = ( ix.ui + ( 0x95f64 << 3 ) ) & 0x800000;
	tx.ui = ix.ui | ( i ^ 0x3f800000 );
	x = tx.zp;
	k += ( i >> 23 );
	f = x - CONST_ONE;
	if( ( 0x007fffff & ( 15 + ix.ui ) ) < 16 ) {	/* |f| < 2**-20 */
		if( f == CONST_ZERO ) {
			if( k == 0 ) {
				return CONST_ZERO;
			}  else {
				dk = as_frizz_fp( k );
				return dk * c[LOG_C_ln2_hi] + dk * c[LOG_C_ln2_lo];
			}
		}
		R = f * f * ( CONST_HALF - c[LOG_C_333] * f );
		if( k == 0 ) {
			return f - R;
		} else {
			dk = as_frizz_fp( k );
			return dk * c[LOG_C_ln2_hi] - ( ( R - dk * c[LOG_C_ln2_lo] ) - f );
		}
	}
	s = f / ( CONST_TWO + f );
	dk = as_frizz_fp( k );
	z = s * s;
	i = ix.ui - ( 0x6147a << 3 );
	w = z * z;
	j = ( 0x6b851 << 3 ) - ix.ui;
	t1 = w * ( c[LOG_C_Lg2] + w * ( c[LOG_C_Lg4] + w * c[LOG_C_Lg6] ) );
	t2 = z * ( c[LOG_C_Lg1] + w * ( c[LOG_C_Lg3] + w * ( c[LOG_C_Lg5] + w * c[LOG_C_Lg7] ) ) );
	i |= j;
	R = t2 + t1;
	if( i > 0 ) {
		hfsq = CONST_HALF * f * f;
		if( k == 0 ) {
			return f - ( hfsq - s * ( hfsq + R ) );
		} else {
			return dk * c[LOG_C_ln2_hi] - ( ( hfsq - ( s * ( hfsq + R ) + dk * c[LOG_C_ln2_lo] ) ) - f );
		}
	} else {
		if( k == 0 ) {
			return f - s * ( f - R );
		} else {
			return dk * c[LOG_C_ln2_hi] - ( ( s * ( f - R ) - dk * c[LOG_C_ln2_lo] ) - f );
		}
	}
}

