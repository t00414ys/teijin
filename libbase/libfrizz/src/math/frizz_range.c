/*!******************************************************************************
 * @file frizz_range.c
 * @brief truncate angle range function
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "frizz_math_common.h"
#include "frizz_math.h"

/**
 * @brief truncate range to [0, +2pi]
 *
 * @param [in] rad in radian
 *
 * @return truncated radian, [0, +2pi]
 */
frizz_fp frizz_0_2pi_range( frizz_fp rad )
{
	while( rad < CONST_ZERO ) {
		rad += FRIZZ_MATH_PI_x2;
	}
	while( FRIZZ_MATH_PI_x2 < rad ) {
		rad -= FRIZZ_MATH_PI_x2;
	}
	return rad;
}

/**
 * @brief truncate range to [-pi, +pi]
 *
 * @param [in] rad in radian
 *
 * @return truncated radian, [-pi, +pi]
 */
frizz_fp frizz_pi_pi_range( frizz_fp rad )
{
	while( rad < -FRIZZ_MATH_PI ) {
		rad += FRIZZ_MATH_PI_x2;
	}
	while( FRIZZ_MATH_PI < rad ) {
		rad -= FRIZZ_MATH_PI_x2;
	}
	return rad;
}

