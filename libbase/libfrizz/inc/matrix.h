/*!******************************************************************************
 * @file  matrix.h
 * @brief For Matrix operation on frizz
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MATRIX_H__
#define __MATRIX_H__
#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

/// for align
#define MATRIX_ALIGN_BYTE 16		///< num of byte for align
#define MATRIX_ALIGN_NUM 4			///< num of units for align
#define MATRIX_ALIGN_NUM_SHIFT 2	///< num of bits for num of align

/**
 * @brief structure for matrix
 */
typedef struct {
	union {
		frizz_fp4w**	vec; 	///< element as simd
		frizz_fp**		data;	///< element as single
	};
	int	rows;	///< row
	int	cols;	///< column
	int	blks;	///< num of simd element in row
} Matrix;

/**
 * @brief create matrix
 *
 * @param [in] rows num of rows
 * @param [in] cols num of columns
 *
 * @return matrix object
 */
Matrix *alloc_matrix( int rows, int cols );

/**
 * @brief delete matrix
 *
 * @param [in] mtx matrix object
 */
void free_matrix( Matrix *mtx );

/**
 * @brief print element of matrix for Debug
 *
 * @param [in] mtx matrix object
 * @param [out] pointer to print strings
 */
void print_matrix( Matrix *mtx, const char *str );

/**
 * @brief setting element of matrix
 *
 * @param [out] mtx matrix object
 * @param [in] src data to set
 */
void set_matrix( Matrix *mtx, const frizz_fp *src );

/**
 * @brief set as identity matrix
 *
 * @param [out] mtx
 *
 * @note please make sure that input is square matrix
 */
void set_matrix_identity( Matrix *mtx );

/**
 * @brief set element of matrix at diag
 *
 * @param [in,out] mtx matrix object
 * @param [in] num num of entry
 * @param [in] entry data tot set element
 */
void set_matrix_diag( Matrix *mtx, int num, const frizz_fp *entry );

/**
 * @brief copy matrix
 *
 * @param [in] src
 * @param [out] dst
 */
void copy_matrix( Matrix *src, Matrix *dst );

/**
 * @brief matrix add
 * dst = src0 + src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 */
void add_matrix( Matrix *src0, Matrix *src1, Matrix *dst );

/**
 * @brief matrix sub
 * dst = src0 - src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 */
void sub_matrix( Matrix *src0, Matrix *src1, Matrix *dst );

/**
 * @brief matrix mul
 * dst = src0 * src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void mul_matrix( Matrix *src0, Matrix *src1, Matrix *dst );

/**
 * @brief matrix mul with transpose
 * dst = src0 * src1^T
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void mul_trmatrix( Matrix *src0, Matrix *src1, Matrix *dst );

/**
 * @brief matrix mul with transpose
 * dst = src0^T * src1
 *
 * @param [in] src0
 * @param [in] src1
 * @param [out] dst
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void mul_trmatrix2( Matrix *src0, Matrix *src1, Matrix *dst );

/**
 * @brief transpose matrix
 * dst = src^T
 *
 * @param [in] src
 * @param [out] dst
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
void transpose_matrix( Matrix *src, Matrix *dst );

/**
 * @brief scale matrix
 * mtx = scalar * matx
 *
 * @param [in,out] mtx
 * @param [in] scalar
 */
void scale_matrix( Matrix *mtx, frizz_fp scalar );

/**
 * @brief invert matrix
 * inv = src^(-1)
 *
 * @param [in] src
 * @param [out] inv
 * @param [out] mtx rows/cols are same src matrix
 *
 * @return 0; success, -1: no answer
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 * @note please don't set same Matrix object between input and output.
 */
int invert_matrix( Matrix *src, Matrix *inv, Matrix *mtx );

/**
 * @brief check wheter or not zero matrix
 *
 * @param [in] mtx
 *
 * @return 0: OK, -1:NG
 *
 * @note checking with 1.0e-5 as zero
 */
int chk_matrix( Matrix *mtx );

/**
 * @brief dot product of vector
 * result = vec0 * vec1
 *
 * @param [in] vec0
 * @param [in] vec1
 *
 * @return result
 *
 * @note please make sure that parameters are correct combination of rows/cols.
 */
frizz_fp inner_product( Matrix *vec0, Matrix *vec1 );

/**
 * @brief cross product of 3-Axis vector
 * dst = vec0 x vec1
 *
 * @param [in] vec0
 * @param [in] vec1
 * @param [out] dst
 *
 * @note please make sure that input of rows are 3 and cols are 1.
 * @note please don't set same Matrix object between input and output.
 */
void vector_product( Matrix *vec0, Matrix *vec1, Matrix *dst );

/**
 * @brief calculate eigenvector at specialization symmetric matx
 * sym * eig_vec = eig_vec * eig_val
 *
 * @param [in] sym symmetric matrix
 * @param [out] eig_vec eigenvector in each column (3x3 matrix)
 * @param [out] eig_val eigenvalue in each diag (3x3 matrix)
 *
 * @note sym * eig_vec[i] = eig_val[i][i] * eig_vec[i]
 * @note please don't set same Matrix object between input and output.
 */
void eigen_matrix( Matrix *sym, Matrix *eig_vec, Matrix *eig_val );

#ifdef __cplusplus
}
#endif

#endif//__MATRIX_H__
