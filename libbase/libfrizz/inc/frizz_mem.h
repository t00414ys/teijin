/*!******************************************************************************
 * @file  frizz_mem.h
 * @brief For dynamic memory allocation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_MEM_H__
#define __FRIZZ_MEM_H__

#ifdef __cplusplus
extern "C" {
#endif

#define FRIZZ_MEM_ALIGN_BYTE 16


/**
 * @brief heap area max point
 *
 */
extern unsigned char *g_frizz_malloc_max;
#define	D_STACK_USE_MARGIN		(0x400)		// heap of margin to be used in the newlib of printf, etc.

/**
 * @brief heap area
 *
 * @note required to setting before the first time execute frizz_malloc() function.
 * (required to setting address)
 */
extern unsigned char *g_frizz_malloc_area;

/**
 * @brief heap area size
 *
 * @note required to setting before the first time execute frizz_malloc() function.
 * (required to setting effective value)
 */
extern unsigned int g_frizz_malloc_size;

/**
 * @brief memory allocate
 *
 * @param size get memory size
 *
 * @return get memory address
 *
 * @note required to declare following area and variable.
 *
 * unsigend char g_frizz_malloc_area[HEAP_SIZE];	// heap area
 *
 * const unsigend int g_frizz_malloc_size = HEAP_SIZE;	// heap size
 */
void *frizz_malloc( unsigned int size );

/**
 * @brief memory free
 *
 * @param p memory area
 * @note empty function
 */
void frizz_free( void *p );

#ifdef __cplusplus
}// extern "C"
#endif

#endif//__FRIZZ_MEM_H__
