/*!******************************************************************************
 * @file    frizz_legacy_substitution.h
 * @brief   frizz legacy code substitution
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __FRIZZ_LEGACY_H__
#define __FRIZZ_LEGACY_H__

#include "frizz_type.h"

#define	frizz_tie_ltcmp(left,right)			( ((left) <  (right)) ? 1 : 0 )
#define	frizz_tie_lecmp(left,right)			( ((left) <= (right)) ? 1 : 0 )
#define	frizz_tie_gtcmp(left,right)			( ((left) >  (right)) ? 1 : 0 )
#define	frizz_tie_gecmp(left,right)			( ((left) >= (right)) ? 1 : 0 )
//
#define	frizz_tie_ssmul(src0,src1)			( (src0) * (src1) )
#define	frizz_tie_ssmul_add(dst,src0,src1)	( (dst) += (src0) * (src1) )
#define	frizz_tie_ssmul_sub(dst,src0,src1)	( (dst) -= (src0) * (src1) )
#define	frizz_tie_ssmad(m0,m1,a0)			( ((m0) * (m1)) + a0 )
#define	frizz_tie_ssadd(src0,src1)			( (src0) + (src1) )
#define	frizz_tie_sssub(src0,src1)			( (src0) - (src1) )
//
#define	frizz_tie_ppmul4w(src0,src1)		( (src0) * (src1) )

#endif	//#ifndef __FRIZZ_LEGACY_H__
