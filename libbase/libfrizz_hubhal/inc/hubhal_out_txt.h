/*!******************************************************************************
 * @file hubhal_out_txt.h
 * @brief source code for hubhal_out_txt.c
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUBHAL_OUT_TXT_H__
#define __HUBHAL_OUT_TXT_H__

#include "quart.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief initialize
 *
 * @param freq UART block clock frequency
 * @param baudrate target baudrate
 */
void hubhal_out_txt_init( unsigned int freq, QUART_BAUDRATE_MODE baudrate );

/**
 * @brief send output data
 *
 * @param sen_id sensor ID
 * @param area sensor area (0 or 1)
 * @param ts timestamp
 * @param num number of data
 * @param data data
 * @param f_block blocking flag(0:disable, !0:enable)
 * @param f_notify interrupt signal flag(0:disable, 1:enable)
 *
 * @return 0: Success, <0: Fail
 */
int hubhal_out_txt_snd_data( unsigned char sen_id, unsigned int area, unsigned int ts, unsigned int num, void *data, int f_block, int f_notify );

/**
 * @brief output response for command
 *
 * @param sen_id sensor ID
 * @param cmd_code CMD Code
 * @param res Response Code
 * @param num Response Data Size
 * @param data Response Data
 */
void hubhal_out_txt_res_cmd( unsigned char sen_id, unsigned int cmd_code, int res, unsigned int num, void *data );

/**
 * @brief ACK
 *
 * @param f_ack 0:NACK, !0:ACK
 */
void hubhal_out_txt_ack( int f_ack, unsigned int mes_uart );

/**
 * @brief output
 *
 * @param [in] int_no 0~3:with falling edge signal of specific GPIO no at event, <0:without interrupt signal
 */
void hubhal_out_txt_set_int_no( int int_no, int level );

#ifdef __cplusplus
}
#endif

#endif//__HUBHAL_OUT_TXT_H__
