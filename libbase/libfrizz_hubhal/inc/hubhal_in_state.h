/*!******************************************************************************
 * @file hubhal_in_state.h
 * @brief source code for hubhal_in_state
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUBHAL_IN_STATE_H__
#define __HUBHAL_IN_STATE_H__

#include "hubhal_format.h"
#include "hubhal_in.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief command reading state
 */
typedef enum {
	HUBHAL_IN_STATE_NONE = 0,		/// idle state
	HUBHAL_IN_STATE_READING,		/// reading state
	HUBHAL_IN_STATE_CMD,			/// command complete
} HUBHAL_IN_STATE;

/**
 * @brief command management information
 */
typedef struct {
	HUBHAL_IN_SOURCE		src;		///< input source
	HUBHAL_IN_STATE			state;		///< command reading state
	hubhal_format_header_t	head;		///< getting command header
	unsigned int			data[64];	///< payload
	unsigned char			num;		///< number of parameter
} hubhal_in_info_t;

extern hubhal_in_info_t g_hubhal_in;

#ifdef __cplusplus
}
#endif

#endif//__HUBHAL_IN_STATE_H__
