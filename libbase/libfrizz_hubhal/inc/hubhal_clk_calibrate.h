/*!******************************************************************************
 * @file  hubhal_clk_calibrate.c
 * @brief to calibrate internal OSC and check calibration value
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef HUBHAL_CLK_CALIBRATE_H
#define HUBHAL_CLK_CALIBRATE_H

#ifdef __cplusplus
extern "C" {
#endif

#include "gpio.h"
#include "frizz_peri.h"

#define	D_CALIB_TIMEOUT_VALUE	(10 * 1000)		///< calibration time out value unit ms
#define	D_PULSE_WIDTH_MAX		(1000)			///< pulse width max value from host to frizz for calibration
#define	D_DEFAULT_ROSC1_FREQ	(100000)		///< default awake frequency
#define	D_DEFAULT_ROSC1_MIN		(65000)			///< -35%
#define	D_DEFAULT_ROSC1_MAX		(135000)		///< +35%
#define	D_DEFAULT_ROSC2_FREQ	(40000000)		///< default core frequency
#define	D_DEFAULT_ROSC2_MIN		(26000000)		///< -35%
#define	D_DEFAULT_ROSC2_MAX		(54000000)		///< +35%

typedef enum {
	CALIB_RET_S_SUCCESS = 0,		///< No error
	CALIB_RET_E_PARAMETERS = -1,	///< parameter error
	CALIB_RET_E_TIMEOUT = -2,		///< calibration timeout
	CALIB_RET_E_OVER_FREQ = -3		///< calibration over frequency
} eCALIB_ERR;

typedef struct {
	volatile unsigned int gpio_cb_count;
	unsigned int rise_cycle_count;
	unsigned int fall_cycle_count;
	unsigned int rise_awake_count;
	unsigned int fall_awake_count;
} st_clk_calibrate_info_t;

typedef struct {
	unsigned int rosc1;
	unsigned int rosc2;
} st_internal_osc_t;


int set_calibration_param( unsigned int pulse_width );
int clk_calibrate( st_internal_osc_t* pre_core_freq );


#ifdef __cplusplus
}
#endif

#endif
