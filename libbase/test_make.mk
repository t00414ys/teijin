#######
#
# frizz subdir Makefile
#

####
# set global macro
ifndef WORKSPACE_LOC
PWD   := $(shell pwd)
WORKSPACE_LOC =$(PWD)/..
endif
include $(WORKSPACE_LOC)/make.rules
include $(WORKSPACE_LOC)/test_make_sub.mk
